#Henry Winn
class Train
	def initialize(sectionsArray)
		@x = nil
		@v = 0.0
		@length = 100 #meters
		@acceleration = (2700.0/3600.0) #meters/second/second
		@deceleration = (-3800.0/3600.0) #meters/second/second
		@topSpeed = (90000.0/3600.0) #meters/sec
		@finished = false
		@currentSection = nil
		@started = false
		@timeAtStation = nil
		#making departure time array. added extra spot in array because the initial departure isn't a section
		@departures = Array.new((sectionsArray.length + 1), nil)
		@arrivals = Array.new(sectionsArray.length, nil)
	end

	def TopSpeed
		@topSpeed
	end

	def getAcceleration
		@acceleration
	end

	def getX
		@x
	end

	def setX!(num)
		@x = num
	end

	def getV
		@v
	end

	def setV!(num)
		@v = num
	end

	def setDeparture!(section, elt)
		@departures[section] = elt
	end

	def setArrival!(section, elt)
		@arrivals[section] = elt
	end

	def finished?
		@finished
	end

	def finish!
		@finished = true
	end

	def getCurrentSection
		@currentSection
	end

	def incrementCurrentSection!
		@currentSection += 1
	end

	def getStopLocation
		(@v*@v)/((-2.0)*@deceleration) + @x
	end

	def started?
		@started
	end

	def stopped?
		if @started
			false
		else
			true
		end
	end

	def start!
		@started = true
		@v = @acceleration
		
		if @x == nil
			@x = @acceleration
		else
			@x = @x + @acceleration
		end
		
		if @currentSection == nil
			@currentSection = 0
		else
			@currentSection += 1
		end
		@timeAtStation = nil
	end

	def stop!
		@started = false
		@v = 0
		@timeAtStation = 0
	end

	def getTimeAtStation
		@timeAtStation
	end

	def incrementTimeAtStation!
		if @timeAtStation.is_a?(Numeric)
			@timeAtStation += 1
		end
	end

	def getTimeAtStation
		@timeAtStation
	end

	def speedUp!
		@v = @v + @acceleration
		@x = @x + @v
	end

	def slowDown!
		@v = @v + @deceleration
		@x = @x + @v
	end

	def coast!
		@x = @x + @v
	end

	def getDepartures
		@departures
	end

	def getArrivals
		@arrivals
	end


end

class Section
	def initialize(num)
		@length = num.to_f
		@occupied = false
	end

	def getLength
		@length
	end

	def getStationX
		@length - 150.0
	end

	def occupied?
		@occupied
	end

	def setOccupied!(bool)
		@occupied = bool
	end
end

def makeFiveChars(input)
	a = input.to_s.split(//)
	while a.length < 5 do
		a.unshift(" ")
	end
	a = a.join
end

params = gets
params = params.split(' ')

numberOfTrains = params[0].to_i

if numberOfTrains > 5 || numberOfTrains < 1
	puts "ERROR"
	raise "ERROR"
end

if params.length > 6
	puts "ERROR"
	raise "ERROR"
end

sectionLengthArray = Array.new
iterator = 0
params.each do |param|
	if iterator == 0
		iterator += 1
	else
		sectionLengthArray << param.to_i
	end
end

sum = 0
sectionLengthArray.each do |num|
	sum += num
end

if sum != 100000
	puts "ERROR"
	raise "ERROR"
end

# sections = []
# sum = sectionLengthArray[0]
# sectionLengthArray.each do |num|
# 	sections << Section.new(sum)
# 	sum += num
# end

# trains = []
# iterator = 0
# while iterator < numberOfTrains do
# 	trains << Train.new(sectionLengthArray)
# 	iterator += 1
# end
sections = []
sections << Section.new(500)
sections << Section.new(1000)
sections << Section.new(100000)

trains = []
trains << Train.new(sections)
trains << Train.new(sections)

time = 1

while trains.last.finished? != true do
	trains.each do |train|
		#if train is not finished
		if train.finished? == false
			#if the train hasn't reached the station yet but has left the beginning
			if train.getTimeAtStation == nil && train.getCurrentSection != nil
				#if the train needs to slow down to stop at the end of the section
				if train.getStopLocation >= sections[train.getCurrentSection].getLength
					train.slowDown!
				end

				#if the train shouldn't start slowing down
				if train.getStopLocation < sections[train.getCurrentSection].getLength
					#if the train's velocity is lower than its top speed, speedUp!, else coast
					if (train.getV + train.getAcceleration) < train.TopSpeed
						train.speedUp!
					else
						train.coast!
					end
				end

				#if the train is at the beginning of the station, set arrival time
				if train.getX >= sections[train.getCurrentSection].getStationX
					train.setArrival!(train.getCurrentSection, time)
				end

				#if the train is pretty much at station, we'll stop
				if  train.getX >= sections[train.getCurrentSection].getLength
					train.stop!
				end
			end
			
			#if the train hasn't started and the first section is not occupied
			if train.started? == false && train.getCurrentSection == nil && sections[0].occupied? == false
				train.start!
				sections[0].setOccupied!(true)
				train.setDeparture!(0, time)
			end
			

			#if the train is at the last stop, set finished to true
			if train.getX != nil && train.getX >= sections.last.getLength
				train.finish!
				sections[train.getCurrentSection].setOccupied!(false)
			end

			#if the train is at the station and hasn't waited long enough
			if train.getX != nil && train.stopped?
				train.incrementTimeAtStation!
			end

			#if train has waited long enough and the next section is not occupied
			if train.getTimeAtStation != nil && train.getTimeAtStation >= 120 && sections[(train.getCurrentSection+1)].occupied? == false
				sections[train.getCurrentSection].setOccupied!(false)
				sections[(train.getCurrentSection+1)].setOccupied!(true)
				train.setDeparture!((train.getCurrentSection+1), time)
				train.start!
			end
		#end if train is not finished
		end
	#end iteration through trains
	end
	time += 1
#while the last train isn't finished
end

trainNumber = 1
trains.each do |train|
	string = trainNumber.to_s + " : ***** - " + makeFiveChars(train.getDepartures[0].to_s) + "  " + makeFiveChars(train.getArrivals[0].to_s)
	iterator = 1
	while train.getArrivals[iterator] != nil do
		string += " - " + makeFiveChars(train.getDepartures[iterator].to_s) + "  " + makeFiveChars(train.getArrivals[iterator].to_s)
		iterator += 1
	end
	string += " *****"
	puts string
	trainNumber += 1
end
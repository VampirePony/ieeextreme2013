def find
   numbers = gets.split(",")
   n = numbers.first.to_i
   m = numbers.last.to_i

   low = n.to_s(2).length
   high = m.to_s(2).length

   total = 0
   for i in low..high
      total = total + rec(i)
   end

   r_low = find_pals(low) # will contain decimal values
   r_high = find_pals(high)

   r_low.each do |i|
      num = i.to_i(2)
      if n > num
         total = total - 1
      end
   end

   r_high.each do |i|
      num = i.to_i(2)
      if m < num
         total = total - 1
      end
   end

   puts total
end

def rec(n)
   if n == 1 or n == 2
      1
   elsif n.even?
      rec(n-1)
   elsif n.odd?
      2**((n-1)/2)
   end
end

def find_pals(n)
   if n > 3
      n = n-2
   
      stop = "1"*n
      stp = stop.to_i(2)
      arr = []

      for i in 0..stp
         tester = ("%0#{n}b" % i)
         if tester == tester.reverse
            inser = ("1" << tester << "1")
            if inser.length == (n+2)
               arr << inser
            end
         end
      end
      arr
   else
      1
   end
end

find

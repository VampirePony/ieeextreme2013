# Network Tree 
# Written by Robert Unverzagt

def find
   n = gets.to_i
   nodes = []

   for i in 1..n # create at least enough nodes
      nodes << Node.new(i)
   end

   for i in 1..(n-1) # edit data in nodes
      input = gets.split(" ")
      nodes[input[0].to_i-1].add_to_to(input[1]) # note that ids must be decremented every time in the future
      nodes[input[1].to_i-1].add_to_from(input[0])
   end

   max = 0 # work backwards to find end of bottleneck seq
   for j in (0..(nodes.length-1)).to_a.reverse
      if nodes[j].to.length > nodes[max].to.length
         max = j
      end
   end

   output = [] # work backwards to find all of bottleneck seq
   nexter = max
   cont = true
   while cont
      output.unshift(nodes[nexter])
      if nodes[nexter].from.length == 1
         nexter = (nodes[nexter].from[0].to_i - 1)
      else
         cont = false
      end
   end

   cont = true
   while cont
      if find_start(nodes.slice!(0..max)) == 0
         cont = false
      else
         output << find_seq(max, nodes)
      end
   end

   output.each do |n|
      puts n.name
   end
end

def find_start(nodes)
   max = 0 # work backwards to find end of bottleneck seq
   for j in (0..(nodes.length-1)).to_a.reverse
      if nodes[j].to.length > nodes[max].to.length
         max = j
      end
   end
   max
end

def find_seq(nexter, nodes)
   output = [] # work backwards to find all of bottleneck seq
   cont = true
   while cont
      output.unshift(nodes[nexter])
      if nodes[nexter].from.length == 1
         nexter = (nodes[nexter].from[0].to_i - 1)
      else
         cont = false
      end
   end
   output
end



class Node
   @name = nil
   @from = []
   @to = []

   def initialize(nm)
      @name = nm
      @from = []
      @to = []
   end

   def name
      @name
   end

   def from
      @from
   end

   def to
      @to
   end

   def change_name(nm)
      @name = nm
   end

   def add_to_from(id)
      @from << id
   end

   def add_to_to(id)
      @to << id
   end
end

find

# Second problem
# Written by Robert Unverzagt


def calc
   input = gets.chomp.split(" ")

   in_num = []
   inc = 0
   until input.length == 1
      input.each do |i|
         if valid_num?(i)
            if i.is_a? String
               in_num << i.hex
            else
               in_num << i
            end
            inc = inc + 1
         else
            inc = inc + 1
            in_num << i
            break
         end
      end
         input.shift(inc)
         input.insert(0, calculation(in_num))
         in_num=[]
         inc = 0
   end

   output = nil.to_s

   if input.first >= 65536 
      output = "FFFF"
   elsif input.first <= 0
      output = "0000"
   else
      output = ("%04X" % input.first.to_i)
   end

   $stdout.print(output)
end


   # Perform logic
def calculation(input)
   if input.length == 1
      $stdout.print("ERROR")
      exit
   end
   if input.length == 2
      if input.last == "~"
         f = input.first.to_s(2).split("")
         o = nil.to_s
         f.each do |i|
            if i == "0"
               o<<"1"
            else
               o<<"0"
            end
         end
         until o.length == 4
            o = ("0" << o)
         end
          abort(o)
      else
      $stdout.print("ERROR")
      exit
      end
   end

   if input.last == "+"
      input.delete_at(input.length-1)
      sum = 0
      input.each do |i|
         sum = sum + i
      end
      sum
   elsif input.last == "-"
     input.delete_at(input.length-1)
     input.first - input.last

   elsif input.last == "&"
      f = input.first.to_s(2).split("")
      s = input[1].to_s(2).split("")
      o = nil.to_s
      l = 0
      while f.length > s.length
         s.unshift("0")
      end
      while s.length > f.length
         f.unshift("0")
      end
      l = f.length
      for j in 0..(l-1)
         if f[j] == s[j] and f[j] == "1"
            o << "1"
         else
            o << "0"
         end
      end
      o = o.to_i(2).to_s(16).capitalize
      until o.length >= 4
         o = ("0" << o)
      end
      $stdout.print(o, "\n")
      exit
   elsif input.last == "|"
      f = input.first.to_s(2).split("")
      s = input[1].to_s(2).split("")
      o = nil.to_s
      l = 0
      while f.length > s.length
         s.unshift("0")
      end
      while s.length > f.length
         f.unshift("0")
      end
      l = f.length
      for j in 0..(l-1)
         if f[j] == s[j] and f[j] == "0"
            o << "0"
         else
            o << "1"
         end
      end
      o = o.to_i(2).to_s(16).capitalize
      until o.length >= 4
         o = ("0" << o)
      end
      $stdout.print(o, "\n")
      exit
   elsif input.last == "X"
      f = input.first.to_s(2).split("")
      s = input[1].to_s(2).split("")
      o = nil.to_s
      l = 0
      if f.length > s.length
         s = ["0"] << s
      elsif s.length > f.length
         f = ["0"] << f
      end
      l = f.length
      for j in 1..l
         if f[-j] == s[-j]
            o = "0" << o
         else
            o = "1" << o
         end
      end
      until o.length >= 4
         o = ("0" << o)
      end
      abort(o)
   end
end

def valid_num?(i)
   if (Integer(i) rescue nil).is_a? Fixnum
      true
   elsif i.hex != 0
      true
   else
      false
   end
end

calc
